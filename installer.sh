apt-get install net-tools sudo -y && \
sudo sed -i 's/#Port 22/Port 22/' /etc/ssh/sshd_config && \
sudo sed -i 's/#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/' /etc/ssh/sshd_config && \
sudo sed -i 's/PermitRootLogin without-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
sudo service ssh restart && \
sudo apt-get install net-tools bzip2 -y && \
sudo apt-get update && sudo apt upgrade -y && \
sudo apt-get install lsb-release apt-transport-https ca-certificates -y && \
sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg && \
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php7.3.list && \
sudo apt-get update && sudo apt upgrade -y && \
sudo apt-get install apache2 apache2-utils sqlite3 php7.3 libapache2-mod-php7.3 php-sqlite3 php-common php7.3-cli php7.3-common php7.3-json php7.3-opcache php7.3-readline php7.3-zip php7.3-gd php7.3-mbstring php7.3-curl php7.3-xml php7.3-bcmath php-intl php-imagick redis-server php7.3-redis php-apcu -y && \
mkdir /var/www/nextcloud-data && \
wget https://download.nextcloud.com/server/releases/latest.tar.bz2 -O /var/www/nextcloud-latest.tar.bz2 && \
tar -xvjf /var/www/nextcloud-latest.tar.bz2 -C /var/www && \
sudo chown -R www-data:www-data /var/www/nextcloud && \
sudo sed -i 's/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/nextcloud/' /etc/apache2/sites-available/000-default.conf && \
sudo sed -i 's/\/var\/www\//\/var\/www\/nextcloud\//' /etc/apache2/apache2.conf && \
sudo sed -i '/<Directory \/var\/www\/nextcloud\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf && \
sudo a2enmod rewrite && \
sudo sed -i 's/max_execution_time = 30/max_execution_time = 30000/' /etc/php/7.3/apache2/php.ini && \
sudo sed -i 's/memory_limit = 128M/memory_limit = 1024M/' /etc/php/7.3/apache2/php.ini && \
sudo sed -i 's/post_max_size = 8M/post_max_size = 200M/' /etc/php/7.3/apache2/php.ini && \
sudo sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 200M/' /etc/php/7.3/apache2/php.ini && \
sudo systemctl restart apache2 && \
VAR=$(ip route get 8.8.8.8 | awk '{ print $NF; exit }'); echo "Open http://"$VAR" in your browser, and complete the installation." && \
read -p "Press any key after installation is complete... " -n1 -s && \
sudo sed -i "/  'installed' => true,/a \  'memcache.local' => '\\\OC\\\Memcache\\\APCu',\n\  'memcache.distributed' => '\\\OC\\\Memcache\\\Redis',\n\  'memcache.locking' => '\\\OC\\\Memcache\\\Redis',\n\  'redis' => [\n\       'host' => '127.0.0.1',\n\       'port' => 6379,\n\  ]," /var/www/nextcloud/config/config.php