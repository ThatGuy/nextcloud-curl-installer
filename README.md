# NextCloud Installer

## Install

**NB. This sssumption is that this is to be run on a completely clean Debian installation. If you try and run it on an existing Server, it is likely to break things**

From the Debian root prompt, run:

`bash <(wget -qO- https://gitlab.com/ThatGuy/nextcloud-curl-installer/raw/master/installer.sh)`

## What does this script do?

In order:

*  apt-get update & upgrade
*  Installs net-tools; bzip2 and sudo
*  Changes sshd_config to force IPv4 and Port 22 only
*  Changes sshd_config to allow root login with password (delete line #5 if you dont want this)
*  Restarts the ssh service
*  Installs lsb-release; apt-transport-https, and ca-certificates
*  Gets the gpg key from https://packages.sury.org/php/apt.gpg (used for php7.3 repo)
*  Adds the https://packages.sury.org/php/ repo to /etc/apt/sources.list.d/php7.3.list
*  apt-get update & upgrade
*  Installs apache2 apache2-utils sqlite3 php7.3 libapache2-mod-php7.3 php-sqlite3 php-common php7.3-cli php7.3-common php7.3-json php7.3-opcache php7.3-readline php7.3-zip php7.3-gd php7.3-mbstring php7.3-curl php7.3-xml php7.3-bcmath php-intl php-imagick redis-server php7.3-redis php-apcu
*  Gets the latest version of Nextcloud from download.nextcloud.com, and puts it /var/www/nextcloud-latest.tar.bz2
*  Extracts the bz2 to /var/www/nextcloud
*  Updates the ownership of /var/www/nextcloud to www-data:www-data
*  Configures /etc/apache2/sites-available/000-default.conf (DocumentRoot)
*  Configures /etc/apache2/apache2.conf (DocumentRoot and AllowOverride All)
*  Enables Apache2 rewrite
*  Configures PHP.ini max_execution to 30000
*  Configures PHP.ini memory_limit to 1024M
*  Configures PHP.ini post_max_size to 200M
*  Configures PHP.ini upload_max_filesize to 200M
*  Restarts Apache2
*  Pauses to let the web configuration be entered manually
*  Updates the Nextcloud config to include Redis and APCu memcaching
  
## Questions

**What's the purpose of this script?**

To get people with minimal experience, up and running quickly with the minimum of effort.  To remove the possibility of failure.  To get Nextcloud working in an environment that will be fast, and efficient for the average home user.  This is NOT intended to be used by veteran admins.

When a newbie sets up a server, invariably they forget things like memcaching, or AllowOverride All etc.  This idea is to remove the guesswork.

**Why didnt you use MySQL or PostgreSQL?**

Good question.  Two reasons.  The first is that both MySQL and PostgreSQL use running services which consume memory even when not in use.  PostgreSQL is particularly bad, using a minimum of 10Mb per open database.  Ideally, this is supposed to be a small footprint installation, using minimal resources, but getting the best performance possible from those resources used.

The second reason, is that I'm lazy.  It's much easier to script SQLite, with no permissions, than a bigger bulkier service.  I actually tried MariaDB and got it working quite well.  Unfortunately, getting 8mb4 support configured via scripting was just overly complicated, and there was no guarantee of a good result. K.I.S.S right?

**It's INSECURE!!!**

What did you expect?  It's not so much "insecure", as it is "not hardened".  There is only so much I am capable of doing in a script.  I cant configure firewalls etc.  I just don't have the knowledge.

**Why did you limit the uploads to 200M?**

Mostly because this is intended for small installations, with only a couple of users.  Most people don't need anything more than a couple of hundred Mb per upload.  Think pictures, the occasional MP3, maybe a few documents.

**What paths/files are used/altered?**

*  /etc/ssh/sshd_config
*  /etc/apt/sources.list.d/php7.3.list
*  /var/www/nextcloud-latest.tar.bz2
*  /var/www/nextcloud/
*  /etc/apache2/sites-available/000-default.conf
*  /etc/apache2/apache2.conf
*  /etc/php/7.3/apache2/php.ini
*  /var/www/nextcloud/config/config.php
*  /var/log/apache2/error.log

**Is there an "UNDO" button/command**

Not as yet.  If there is enough interest, I might create an uninstall script.

**Why doesn't the script install an email server or (insert application/service here)**

Mostly, because those are separate issues.  They will require more focus than what this script can provide.  NextCloud is going to be the application that pulls everything together, so now we have it up and running, we can look at installing all of the plugins and addon services.